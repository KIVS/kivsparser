/*
 * parse.h
 *
 *  Created on: 28.10.2013
 *      Author: Marc
 */

#ifndef PARSE_H_
#define PARSE_H_

#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int Timestamp;
	double times[10];
	double min;
	double max;
	double avg;
} SAMPLE;

SAMPLE* readSample(FILE* f);
void printSample(FILE* out, SAMPLE* s, int number);
void printSampleHeader(FILE* out);

#endif /* PARSE_H_ */
