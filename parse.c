/*
 * parse.c
 *
 *  Created on: 28.10.2013
 *      Author: Marc
 */

#include "parse.h"

/**
* @brief gives pointer to lower argument
*
* @param a first argument
* @param b second argument
*
* @return pointer to lower argument
*/
inline double* min(double* a, double* b)
{
	return *a<*b ? a : b;
}

/**
* @brief swaps 2 doubles
*
* @param a first argument
* @param b second argument
*/
inline void swap(double* a, double* b)
{
	double tmp;
	tmp=*a;
	*a=*b;
	*b=tmp;
}

/**
* @brief sorts double array in place
*
* @param start pointer to first element
* @param len length of array
*/
void sortArray(double* start, int len)
{
	double* minimum=start;
	int i;
	if(len==1) return;
	for(i=1;i<len;i++) minimum=min(minimum, &start[i]);
	swap(start, minimum);
	sortArray(start+1,len-1);
}

/**
* @brief reads a sample from f
*
* @param f filepointer to read from
*
* @return sample structure (you need to free this!)
*/
SAMPLE* readSample(FILE* f)
{
	SAMPLE* s = malloc(sizeof(*s));
	int i;
	if(!s) return s;

	fscanf(f, "%i\n", &s->Timestamp);
	fscanf(f, "%*[^\n]\n");
	for(i=0;i<10;i++)
		fscanf(f, "%*[^=]=%*[^=]=%*[^=]=%lf ms\n", &(s->times[i]));
	fscanf(f, "\n");
	fscanf(f, "%*[^\n]\n");
	fscanf(f, "%*[^\n]\n");
	fscanf(f, "%*[^=] = %lf/%lf/%lf ms\n",&(s->min),&(s->avg),&(s->max));

	sortArray(s->times, 10);

	return s;
}

/**
* @brief prints a sample
*
* @param out filepointer to output file
* @param s sample to print
* @param number the ongoing number of the current sample
*/
void printSample(FILE* out, SAMPLE* s, int number)
{
	int i;
	fprintf(out, "%i %f %f %f %f %f %f", s->Timestamp,s->min,s->avg,s->max,s->times[2],((s->times[4]+s->times[5])/2),s->times[7]);
	for(i=0;i<10;i++) fprintf(out, " %f", s->times[i]);
	fprintf(out, " %i\n", number);
}

/**
* @brief prints a header for the sample table
*
* @param out filepointer to output file
*/
void printSampleHeader(FILE* out)
{
	fprintf(out, "timestamp min avg max q1 q2 q3 1 2 3 4 5 6 7 8 9 10 #\n");
}
