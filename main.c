/*
 * main.c
 *
 *  Created on: 17.10.2013
 *      Author: Marc
 */
#include <stdio.h>
#include "parse.h"

#define INFILE "raw.dat"
#define OUTFILE "out.csv"

int main()
{
	FILE* in;
	FILE* out;
	SAMPLE* s;
	int j=0;

	if((in=fopen(INFILE, "r"))==NULL)
		return 1;

	if((out=fopen(OUTFILE, "w"))==NULL)
		return 2;

	for(;!feof(in);j++)
	{
		s=readSample(in);
		printSample(out, s, j);
		free(s);
	}


	fclose(in);
	fclose(out);

	return 0;
}
